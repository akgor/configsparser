package cfgmetadata

import (
	"encoding"
	"fmt"
	"reflect"
	"time"
)

type Converter interface {
	Applies(value reflect.Value) bool
	StoreString(value reflect.Value, s string) error
	ToString(value reflect.Value) (string, error)
}

//
// DurationConverter
//

var durationType = reflect.TypeOf(time.Duration(0))

type DurationConverter struct{}

func (_ DurationConverter) Applies(value reflect.Value) bool {
	return value.Type() == durationType
}

func (_ DurationConverter) StoreString(value reflect.Value, s string) error {
	var d time.Duration
	d, err := time.ParseDuration(s)
	if err != nil {
		return err
	}
	value.Set(reflect.ValueOf(d))
	return nil
}

func (_ DurationConverter) ToString(value reflect.Value) (string, error) {
	return value.Interface().(fmt.Stringer).String(), nil
}

//
// TextMarshallerConverter
//

func valueInterface(value reflect.Value) interface{} {
	if value.CanAddr() {
		return value.Addr().Interface()
	} else {
		return value.Interface()
	}
}

type TextMarshallerConverter struct{}

func (TextMarshallerConverter) Applies(value reflect.Value) bool {
	i := valueInterface(value)
	if _, ok := i.(encoding.TextMarshaler); !ok {
		return false
	}
	if _, ok := i.(encoding.TextUnmarshaler); !ok {
		return false
	}
	return true
}

func (TextMarshallerConverter) StoreString(value reflect.Value, s string) error {
	unmarshaller := valueInterface(value).(encoding.TextUnmarshaler)
	return unmarshaller.UnmarshalText([]byte(s))
}

func (TextMarshallerConverter) ToString(value reflect.Value) (string, error) {
	marshaller := valueInterface(value).(encoding.TextMarshaler)
	res, err := marshaller.MarshalText()
	if err != nil {
		return "", err
	}
	return string(res), nil
}

//
// Converters
//

type Converters []Converter

func (c Converters) ConvertToString(value reflect.Value) (bool, string, error) {
	for _, conv := range c {
		if conv.Applies(value) {
			s, err := conv.ToString(value)
			return true, s, err
		}
	}

	return false, "", nil
}

func (c Converters) LoadFromString(value reflect.Value, s string) (bool, error) {
	for _, conv := range c {
		if conv.Applies(value) {
			err := conv.StoreString(value, s)
			return true, err
		}
	}

	return false, nil
}

func (c Converters) Append(cs ...Converter) Converters {
	newConv := make(Converters, len(c), len(c)+len(cs))
	_ = copy(newConv, c)
	newConv = append(newConv, cs...)
	return newConv
}

//
// Default converters list
//

var defaultConverters = Converters{
	DurationConverter{},
	TextMarshallerConverter{},
}
