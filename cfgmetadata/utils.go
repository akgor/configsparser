package cfgmetadata

import (
	"io"
	"reflect"
	"strings"
	"text/tabwriter"
	"text/template"
)

type Loader interface {
	Load() error
}

func Apply(loaders ...Loader) error {
	for _, loader := range loaders {
		if err := loader.Load(); err != nil {
			return err
		}
	}
	return nil
}

// PathKeyMaker joins names of struct fields with dot character.
func PathKeyMaker(s []string, tag *reflect.StructTag) (string, error) {
	return strings.Join(s, fullKeySeparator), nil
}

func TagKeyMaker(tagName string, autoNaming bool) func([]string, *reflect.StructTag) (string, error) {
	return func(s []string, tag *reflect.StructTag) (string, error) {
		if tagName != "" {
			if val, ok := tag.Lookup(tagName); ok {
				if val == "-" {
					return "", nil
				}
				return val, nil
			}
		}
		if autoNaming {
			return PathKeyMaker(s, tag)
		}
		return "", nil
	}
}

type replaceWriter struct {
	w io.Writer

	replacer *strings.Replacer
}

func newReplaceWriter(w io.Writer, oldnew ...string) *replaceWriter {
	return &replaceWriter{
		w:        w,
		replacer: strings.NewReplacer(oldnew...),
	}
}

func (w *replaceWriter) Write(buf []byte) (int, error) {
	input := string(buf)
	output := w.replacer.Replace(input)

	_, err := w.w.Write([]byte(output))
	return len(buf), err
}

func tableHeader(sep byte, columns ...string) string {
	dashes := make([]string, len(columns))
	for n, l := range columns {
		dashes[n] = strings.Repeat(string(sep), len(l))
	}
	return strings.Join(columns, "\t") + "\n" + strings.Join(dashes, "\t")
}

func PrintFieldsWithTemplate(m *MetaData, w io.Writer, t string, fmap map[string]interface{}) (err error) {
	tmpl := template.New("").Funcs(template.FuncMap{
		"tableHeader": tableHeader,
	})

	if fmap != nil {
		tmpl = tmpl.Funcs(fmap)
	}

	tmpl, err = tmpl.Parse(t)
	if err != nil {
		return
	}

	tabWriter := tabwriter.NewWriter(w, 0, 0, 1, ' ', 0)
	defer tabWriter.Flush()

	replaceWriter := newReplaceWriter(tabWriter, "\\t", "\t")

	err = tmpl.Execute(replaceWriter, map[string]interface{}{
		"Fields": m.GetFields(),
	})
	if err != nil {
		return
	}
	return
}
