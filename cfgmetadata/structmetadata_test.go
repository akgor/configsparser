package cfgmetadata_test

import (
	"encoding/json"
	"fmt"
	"net"
	"strings"
	"testing"
	"time"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
)

//
// Helpers
//

func getTestMetadata(t *testing.T, st interface{}, opts *cfgmetadata.Options) *cfgmetadata.MetaData {
	t.Helper()

	if opts == nil {
		opts = &cfgmetadata.Options{}
	}

	m, err := cfgmetadata.NewMetadata(st, *opts)
	if err != nil {
		t.Fatal(err)
	}

	return m
}

func setTestValue(t *testing.T, m *cfgmetadata.MetaData, k, v string, errChecker func(string) bool) {
	t.Helper()

	e := m.SetFieldValue(k, v)
	if errChecker != nil {
		if e != nil {
			se := fmt.Sprint(e)
			if !errChecker(se) {
				t.Errorf("Wrong error: `%s`", se)
				return
			}
			return
		} else {
			t.Errorf("Error was not raised")
		}
	} else {
		if e != nil {
			t.Errorf("Unexpected error was raised: `%s`", e)
		}
	}
}

func errorMatchChecker(s string) func(string) bool {
	return func(err string) bool {
		return s == err
	}
}

func errorContainsChecker(s string) func(string) bool {
	return func(err string) bool {
		return strings.Contains(err, s)
	}
}

//
// Tests
//

func Test_SetSubStructures(t *testing.T) {
	var st struct {
		Sub struct {
			SubSub struct {
				Int          int
				UndefinedInt *int
			}
		}
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "Sub.SubSub.Int", "42", nil)
	if st.Sub.SubSub.Int != 42 {
		t.Errorf("Wrong value: %#v", st.Sub.SubSub.Int)
	}

	if st.Sub.SubSub.UndefinedInt != nil {
		t.Errorf("Wrong value: %#v", st.Sub.SubSub.UndefinedInt)
	}
}

func Test_SetNetIp(t *testing.T) {
	var st struct {
		Ip net.IP
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "Ip", "1.2.3.4", nil)
	if !st.Ip.Equal(net.IPv4(1, 2, 3, 4)) {
		t.Errorf("%#v", st.Ip)
	}

	setTestValue(t, m, "Ip", "1.2.3.444", errorMatchChecker("cannot set field Ip: invalid IP address: 1.2.3.444"))
	if !st.Ip.Equal(net.IPv4(1, 2, 3, 4)) {
		t.Errorf("%#v", st.Ip)
	}
}

func Test_SetDuration(t *testing.T) {
	var st struct {
		Timeout time.Duration
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "Timeout", "2h", nil)
	if st.Timeout != time.Hour*2 {
		t.Errorf("%#v", st.Timeout)
	}

	setTestValue(t, m, "Timeout", "2d", errorContainsChecker(`cannot set field Timeout: time: unknown unit`))
}

func Test_SetBoolPtr(t *testing.T) {
	var st struct {
		SomeValue     *bool
		SomeNullValue *bool
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "SomeValue", "true", nil)
	if st.SomeNullValue != nil {
		t.Errorf("%#v", st.SomeValue)
	}
	if st.SomeValue == nil {
		t.Errorf("%#v", st.SomeValue)
	}
	if !*st.SomeValue {
		t.Errorf("%#v", *st.SomeValue)
	}
}

func Test_SetStringPtr(t *testing.T) {
	var st struct {
		SomeValue *string
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "SomeValue", "foobar", nil)
	if st.SomeValue == nil {
		t.Errorf("%#v", st.SomeValue)
	}
	if *st.SomeValue != "foobar" {
		t.Errorf("%#v", st.SomeValue)
	}
}

func Test_SetJsonIntSlice(t *testing.T) {
	var st struct {
		Arr []int
	}

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		ComplexFieldsUnmarshaler: json.Unmarshal,
		ComplexFieldsMarshaler:   json.Marshal,
	})

	setTestValue(t, m, "Arr", "[1,2,42]", nil)
	if len(st.Arr) != 3 || st.Arr[0] != 1 || st.Arr[1] != 2 || st.Arr[2] != 42 {
		t.Errorf("%#v", st.Arr)
	}
}

func Test_SetNilSubStruct(t *testing.T) {
	type sub struct {
		Value string
	}

	var st struct {
		Sub  *sub
		Sub2 *sub
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "Sub.Value", "test1", nil)
	if st.Sub == nil {
		t.Errorf("%#v", st.Sub)
	}
	if st.Sub.Value != "test1" {
		t.Errorf("%#v", st.Sub.Value)
	}
	// Sub2 is not defined, so it have to be nil
	if st.Sub2 != nil {
		t.Errorf("%#v", st.Sub2)
	}
}

func Test_SetBool(t *testing.T) {
	var st struct {
		SomeValue  bool
		SomeValue1 bool
		SomeValue2 bool
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "SomeValue", "t", nil)
	setTestValue(t, m, "SomeValue1", "false", nil)
	setTestValue(t, m, "SomeValue2", "1", nil)
	if st.SomeValue != true {
		t.Errorf("%#v", st.SomeValue)
	}
	if st.SomeValue1 != false {
		t.Errorf("%#v", st.SomeValue1)
	}
	if st.SomeValue2 != true {
		t.Errorf("%#v", st.SomeValue2)
	}
}

func Test_SetBoolWrongValue(t *testing.T) {
	var st struct {
		SomeValue bool
	}

	m := getTestMetadata(t, &st, nil)
	setTestValue(t, m, "SomeValue", "boo", errorMatchChecker(`cannot set field SomeValue: strconv.ParseBool: parsing "boo": invalid syntax`))
}

func Test_SetInt32(t *testing.T) {
	var st struct {
		SomeValue int32
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "SomeValue", "11", nil)
	if st.SomeValue != int32(11) {
		t.Errorf("%#v", st.SomeValue)
	}
}

func Test_SetFloat32(t *testing.T) {
	var st struct {
		SomeValue float32
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "SomeValue", "11.1", nil)
	if st.SomeValue != float32(11.1) {
		t.Errorf("%#v", st.SomeValue)
	}
}

func Test_SetFloat64(t *testing.T) {
	var st struct {
		SomeValue float64
	}

	m := getTestMetadata(t, &st, nil)

	setTestValue(t, m, "SomeValue", "11.1", nil)
	if st.SomeValue != float64(11.1) {
		t.Errorf("%#v", st.SomeValue)
	}
}

func Test_SetFields(t *testing.T) {
	var st struct {
		String string
		Int    int
		Sub    struct {
			String string
		}
		JsonSub struct {
			String     string
			JsonSubSub struct {
				Int           int
				PredefinedInt int
			}
		}
	}

	st.JsonSub.JsonSubSub.PredefinedInt = 142

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		AllowParsingSubStructures: true,

		ComplexFieldsUnmarshaler: json.Unmarshal,
		ComplexFieldsMarshaler:   json.Marshal,
	})

	setTestValue(t, m, "String", "Bar", nil)
	setTestValue(t, m, "Int", "42", nil)
	setTestValue(t, m, "Sub.String", "111", nil)
	setTestValue(t, m, "JsonSub", `{"String": "test", "JsonSubSub": {"Int": 42}}`, nil)

	if st.String != "Bar" {
		t.Errorf("%#v", st.String)
	}
	if st.Sub.String != "111" {
		t.Errorf("%#v", st.Sub.String)
	}
	if st.Int != 42 {
		t.Errorf("%#v", st.Int)
	}
	if st.JsonSub.String != "test" {
		t.Errorf("%#v", st.JsonSub.String)
	}
	if st.JsonSub.JsonSubSub.Int != 42 {
		t.Errorf("%#v", st.JsonSub.JsonSubSub.Int)
	}
	if st.JsonSub.JsonSubSub.PredefinedInt != 142 {
		t.Errorf("%#v", st.JsonSub.JsonSubSub.PredefinedInt)
	}
}

func Test_EmbeddedStruct(t *testing.T) {
	type Anon struct {
		AnonInt int
	}

	var st struct {
		Anon
		Int int
	}

	m := getTestMetadata(t, &st, nil)

	t.Log(m.GetKeys())

	setTestValue(t, m, "Int", "42", nil)
	setTestValue(t, m, "AnonInt", "42", nil)

	if st.Int != 42 {
		t.Errorf("%#v", st.Int)
	}
	if st.AnonInt != 42 {
		t.Errorf("%#v", st.Int)
	}
}

func Test_TagKeyMaker(t *testing.T) {
	var st struct {
		Int           int `conf:"integer"`
		AutoFieldName int
		SkipMe        int `conf:"-"`
	}

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		KeyMakerFunc: cfgmetadata.TagKeyMaker("conf", true),
	})

	keysStr := fmt.Sprint(m.GetKeys())
	if keysStr != `[AutoFieldName integer]` {
		t.Errorf("%#v", keysStr)
	}

	m = getTestMetadata(t, &st, &cfgmetadata.Options{
		KeyMakerFunc: cfgmetadata.TagKeyMaker("conf", false),
	})

	keysStr = fmt.Sprint(m.GetKeys())
	if keysStr != `[integer]` {
		t.Errorf("%#v", keysStr)
	}
}

func Test_SubstructureUnmarsheller(t *testing.T) {
	var st struct {
		Sub struct {
			Int int
			Str string
		} `conf:"subst"`
	}

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		ComplexFieldsUnmarshaler:  json.Unmarshal,
		ComplexFieldsMarshaler:    json.Marshal,
		AllowParsingSubStructures: true,
		KeyMakerFunc:              cfgmetadata.TagKeyMaker("conf", false),
	})

	keysStr := fmt.Sprint(m.GetKeys())
	if keysStr != `[subst]` {
		t.Errorf("%#v", keysStr)
	}

	setTestValue(t, m, "subst", `{"Int": 42, "Str": "42"}`, nil)

	if st.Sub.Int != 42 {
		t.Errorf("%#v", st.Sub.Int)
	}

	if st.Sub.Str != "42" {
		t.Errorf("%#v", st.Sub.Str)
	}

	f := m.GetField("subst")
	if f == nil {
		t.Errorf("field is nil")
	}

	s := f.StringValue()
	if s != `{"Int":42,"Str":"42"}` {
		t.Errorf("%#v", s)
	}
}

func Test_MapUnmarshaler(t *testing.T) {
	var st struct {
		Map map[string]struct {
			Int int
		} `conf:"themap"`
	}

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		ComplexFieldsUnmarshaler: json.Unmarshal,
		ComplexFieldsMarshaler:   json.Marshal,
		KeyMakerFunc:             cfgmetadata.TagKeyMaker("conf", false),
	})

	keysStr := fmt.Sprint(m.GetKeys())
	if keysStr != `[themap]` {
		t.Errorf("%#v", keysStr)
	}

	setTestValue(t, m, "themap", `{"One":{"Int": 1},"Two":{"Int":2}}`, nil)

	oneVal := st.Map["One"]
	if oneVal.Int != 1 {
		t.Errorf("%#v", oneVal.Int)
	}

	twoVal := st.Map["Two"]
	if twoVal.Int != 2 {
		t.Errorf("%#v", twoVal.Int)
	}

	f := m.GetField("themap")
	if f == nil {
		t.Errorf("field is nil")
	}

	s := f.StringValue()
	if s != `{"One":{"Int":1},"Two":{"Int":2}}` {
		t.Errorf("%#v", s)
	}
}

func Test_NotOwerwriteMaps(t *testing.T) {
	st := struct {
		M map[string]int `conf:"a-map"`
	}{
		M: map[string]int{
			"42": 42,
		},
	}

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		ComplexFieldsUnmarshaler: json.Unmarshal,
		ComplexFieldsMarshaler:   json.Marshal,

		KeyMakerFunc:  cfgmetadata.TagKeyMaker("conf", false),
		OverwriteMaps: false,
	})

	setTestValue(t, m, "a-map", `{"777": 777}`, nil)

	val42 := st.M["42"]
	if val42 != 42 {
		t.Errorf("%#v", val42)
	}

	val777 := st.M["777"]
	if val777 != 777 {
		t.Errorf("%#v", val777)
	}
}

func Test_OwerwriteMaps(t *testing.T) {
	st := struct {
		M map[string]int `conf:"a-map"`
	}{
		M: map[string]int{
			"42": 42,
		},
	}

	m := getTestMetadata(t, &st, &cfgmetadata.Options{
		ComplexFieldsUnmarshaler: json.Unmarshal,
		ComplexFieldsMarshaler:   json.Marshal,

		KeyMakerFunc:  cfgmetadata.TagKeyMaker("conf", false),
		OverwriteMaps: true,
	})

	t.Logf("Value before: %v", st)

	setTestValue(t, m, "a-map", `{"777": 777}`, nil)

	t.Logf("Value after: %v", st)

	_, ok := st.M["42"]
	if ok {
		t.Errorf("key '42' should be missing")
	}

	val777 := st.M["777"]
	if val777 != 777 {
		t.Errorf("%#v", val777)
	}
}
