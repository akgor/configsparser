package cfgmetadata

import (
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

//
// Constants and definitions
//

const fullKeySeparator = "."

//
// Options
//

type Options struct {
	// Generates key which will be used for fields lookup
	KeyMakerFunc func([]string, *reflect.StructTag) (string, error)

	// If set to true, sub structures can be defined using string representation.
	// The format of string is defined by ComplexFieldsUnmarshaler
	AllowParsingSubStructures bool

	// Function which will unmarshal sub structures, slice or maps.
	ComplexFieldsUnmarshaler func([]byte, interface{}) error

	// Function which will marshal sub structures, slices or maps.
	// Usually is used for string representations.
	ComplexFieldsMarshaler func(interface{}) ([]byte, error)

	Loggerf func(string, ...interface{})

	// If true, maps will be owerwritten by making new map,
	// In opposite case they will be updated
	OverwriteMaps bool
}

//
// MetaDataField
//

type MetaDataField struct {
	m *MetaData
	f *structMetaDataField
}

func (f *MetaDataField) setValue(s string) (err error) {
	f.lookupFieldValue(true)

	target := f.f.value
	tt := target.Type()

	ok, err := f.m.converters.LoadFromString(target, s)
	if ok {
		return
	}

	switch target.Kind() {
	case reflect.String:
		target.SetString(s)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		var i int64
		i, err = strconv.ParseInt(s, 10, tt.Bits())
		if err != nil {
			return
		}
		target.SetInt(i)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		var i uint64
		i, err = strconv.ParseUint(s, 10, tt.Bits())
		if err != nil {
			return
		}
		target.SetUint(i)
	case reflect.Float32, reflect.Float64:
		var f float64
		f, err = strconv.ParseFloat(s, tt.Bits())
		if err != nil {
			return
		}
		target.SetFloat(f)
	case reflect.Bool:
		var b bool
		s = strings.ToLower(strings.TrimSpace(s))
		b, err = strconv.ParseBool(s)
		if err != nil {
			return
		}
		target.SetBool(b)
	case reflect.Map:
		if f.m.o.OverwriteMaps {
			newVal := reflect.MakeMap(target.Type())
			target.Set(newVal)
		}
		fallthrough
	case reflect.Struct, reflect.Slice:
		if f.m.o.ComplexFieldsUnmarshaler == nil {
			return fmt.Errorf("complexFieldsUnmarshaler is not defined")
		}
		err = f.m.o.ComplexFieldsUnmarshaler([]byte(s), target.Addr().Interface())
	default:
		err = fmt.Errorf("wrong data type")
	}
	return
}

func (f *MetaDataField) Value() interface{} {
	return f.f.value.Interface()
}

func (f *MetaDataField) StringValue() string {
	f.lookupFieldValue(false)

	target := f.f.value

	if target.Kind() == reflect.Ptr {
		if target.IsNil() {
			return "<nil>"
		}
		target = target.Elem()
	}

	ok, s, err := f.m.converters.ConvertToString(target)
	if ok {
		if err != nil {
			f.m.logf("[ERROR] cannot convert to string: %s", err)
			return "<N/A>"
		}
		return s
	}

	switch target.Kind() {
	case reflect.String:
		return target.String()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return strconv.FormatInt(target.Int(), 10)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return strconv.FormatUint(target.Uint(), 10)
	case reflect.Float32, reflect.Float64:
		return fmt.Sprintf("%f", target.Float())
	case reflect.Bool:
		return strconv.FormatBool(target.Bool())
	case reflect.Struct, reflect.Slice, reflect.Map:
		if f.m.o.ComplexFieldsMarshaler == nil {
			f.m.logf("[ERROR] ComplexFieldsMarshaler is not defined")
			return "<N/A>"
		}
		res, err := f.m.o.ComplexFieldsMarshaler(target.Interface())
		if err != nil {
			f.m.logf("[ERROR] cannot marshal sub structure or slice: %s", err)
			return "<N/A>"
		}
		return string(res)

	default:
		return "<N/A>"
	}
}

func (f *MetaDataField) IsNil() bool {
	f.lookupFieldValue(false)
	target := f.f.value
	return target.Kind() == reflect.Ptr && target.IsNil()
}

// lookupFieldValue finds actual values of fields.
// By default structMetaDataField contains invalid values.
// This method is used to populate structMetaDataField.value with actual value
// and (if create is set) create values for nil fields or structures.
func (f *MetaDataField) lookupFieldValue(create bool) {
	if f.f.value.Kind() != reflect.Invalid {
		return
	}

	// taking root value as current value
	val := f.m.rv

	for _, name := range f.f.path {
		if val.Kind() == reflect.Struct {
			val = val.FieldByName(name)
		}

		if create && val.Kind() == reflect.Ptr {
			if val.IsNil() {
				val.Set(reflect.New(val.Type().Elem()))
			}
			val = val.Elem()
		}
	}

	f.f.value = val
}

func (f *MetaDataField) Key() string {
	return f.f.key
}

func (f *MetaDataField) FullKey() string {
	return f.f.fullKey
}

func (f *MetaDataField) Tag(key string) string {
	return f.f.tag.Get(key)
}

func (f *MetaDataField) Type() reflect.Type {
	return f.f.tp
}

//
// structMetaDataField
//

type structMetaDataField struct {
	key     string
	fullKey string
	path    []string
	tp      reflect.Type
	value   reflect.Value
	tag     reflect.StructTag
}

//
// MetaData
//

type MetaData struct {
	o      Options
	i      interface{}
	rv     reflect.Value // root Value
	fields map[string]*structMetaDataField

	converters Converters
}

func NewMetadata(i interface{}, o Options) (m *MetaData, err error) {
	if o.KeyMakerFunc == nil {
		o.KeyMakerFunc = PathKeyMaker
	}

	val := reflect.ValueOf(i)
	if val.Kind() != reflect.Ptr {
		return nil, fmt.Errorf("object must be sent by reference")
	}

	val = reflect.Indirect(val)
	if val.Kind() != reflect.Struct {
		err = fmt.Errorf("object have to be struct")
		return
	}

	m = &MetaData{
		fields:     make(map[string]*structMetaDataField),
		o:          o,
		i:          i,
		rv:         val,
		converters: defaultConverters,
	}

	err = m.fillFields(nil, reflect.Indirect(val).Type())

	return
}

func (m *MetaData) logf(format string, v ...interface{}) {
	if m.o.Loggerf != nil {
		m.o.Loggerf(format, v...)
	}
}

func (m *MetaData) fillFields(path []string, t reflect.Type) (err error) {
	for n := 0; n < t.NumField(); n++ {
		ft := t.Field(n)

		// unexported field
		if ft.PkgPath != "" {
			continue
		}

		curPath := make([]string, len(path)+1)
		_ = copy(curPath, path)
		curPath[len(path)] = ft.Name

		tp := ft.Type

		if tp.Kind() == reflect.Ptr {
			tp = tp.Elem()
		}

		var key string
		key, err = m.o.KeyMakerFunc(curPath, &ft.Tag)
		if err != nil {
			return err
		}

		if tp.Kind() != reflect.Struct || (m.o.AllowParsingSubStructures && key != "") {
			if key == "" {
				continue
			}

			if _, has := m.fields[key]; has {
				return fmt.Errorf("key `%s` is already exist", key)
			}

			field := &structMetaDataField{
				key:     key,
				fullKey: strings.Join(curPath, fullKeySeparator),
				tp:      ft.Type,
				// Set nil value. It will be assigned later.
				value: reflect.ValueOf(nil),
				tag:   ft.Tag,
				path:  curPath,
			}

			m.fields[key] = field
		}

		if tp.Kind() == reflect.Struct {

			if ft.Anonymous {
				curPath = curPath[0 : len(curPath)-1]
			}

			err = m.fillFields(curPath, tp)
			if err != nil {
				return
			}
			continue
		}
	}

	return
}

func (m *MetaData) GetKeys() []string {
	keys := make([]string, 0, len(m.fields))
	for k := range m.fields {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	return keys
}

func (m *MetaData) GetObject() interface{} {
	return m.i
}

func (m *MetaData) getField(key string) (*MetaDataField, error) {
	f, has := m.fields[key]
	if !has {
		return nil, fmt.Errorf("field with key `%s` does not exist", key)
	}
	return &MetaDataField{m, f}, nil
}

func (m *MetaData) GetField(key string) *MetaDataField {
	f, err := m.getField(key)
	if err != nil {
		m.logf("[ERROR] GetField: error is %s", err)
		return nil
	}
	return f
}

func (m *MetaData) SetFieldValue(key string, value string) error {
	f, err := m.getField(key)
	if err != nil {
		return err
	}
	err = f.setValue(value)
	if err != nil {
		return fmt.Errorf("cannot set field %s: %w", f.f.fullKey, err)
	}
	return nil
}

func (m *MetaData) GetFields() []MetaDataField {
	res := make([]MetaDataField, len(m.fields))
	for n, k := range m.GetKeys() {
		res[n] = MetaDataField{
			f: m.fields[k],
			m: m,
		}
	}
	return res
}

func (m *MetaData) AddConverters(converters ...Converter) *MetaData {
	m.converters = m.converters.Append(converters...)
	return m
}
