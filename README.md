Configsparser is a library that loads configuration parameters from different sources and applies it to a configuration structure.

It supports different sources such as configuration files, environment variables and command line flags.

See `examples` directory for example of usage.
