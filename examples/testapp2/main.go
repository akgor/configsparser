package main

import (
	"encoding/json"
	"flag"
	"log"
	"net"
	"os"
	"time"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"bitbucket.org/akgor/configsparser/envloader"
	"bitbucket.org/akgor/configsparser/fileloader/jsonloader"
	"bitbucket.org/akgor/configsparser/fileloader/tomlloader"
	"bitbucket.org/akgor/configsparser/flagsloader"
)

var (
	printJson     bool
	printToml     bool
	printEnv      bool
	printEnvTable bool
)

type HttpConfig struct {
	Host    string        `desc:"Some description 1"`
	Port    int           `desc:"Some description 2"`
	TLS     bool          `desc:"Some description 3" conf:"http-tls-support"`
	Timeout time.Duration `desc:"Timeout on http call"`
}

type DbConfig struct {
	Host string `desc:"Some description 4"`
	Port uint   `desc:"Some description 5"`
	User string `desc:"Some description 6"`
	Pass string `desc:"Some description 7"`
}

type ExternalLoggerConfig struct {
	Url *string
}

type Config struct {
	Db        DbConfig
	Http      HttpConfig
	ExtLogger *ExternalLoggerConfig

	ListenIp net.IP `desc:"Serve test server on ip"`

	SomeIds []int `conf:"ids" desc:"Just a slice of integers"`
}

func makeFlagSet() *flag.FlagSet {
	flagSet := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	flagSet.BoolVar(
		&printJson,
		"print-cfg-json",
		false,
		"Print json config and exit.",
	)
	flagSet.BoolVar(
		&printToml,
		"print-cfg-toml",
		false,
		"Print toml config and exit.",
	)
	flagSet.BoolVar(
		&printEnv,
		"print-env",
		false,
		"Print env variables and exit.",
	)
	flagSet.BoolVar(
		&printEnvTable,
		"print-env-table",
		false,
		"Print env variables table and exit.",
	)

	return flagSet
}

func main() {
	c := &Config{}

	metaData, err := cfgmetadata.NewMetadata(c, cfgmetadata.Options{
		KeyMakerFunc:             cfgmetadata.TagKeyMaker("conf", true),
		ComplexFieldsMarshaler:   json.Marshal,
		ComplexFieldsUnmarshaler: json.Unmarshal,
		Loggerf:                  log.Printf,
	})
	if err != nil {
		log.Fatal(err)
	}

	flagSet := makeFlagSet()

	flagsCfg := flagsloader.
		NewLoader(metaData).
		WithDescriptionKey("desc").
		WithFlagsSet(flagSet)

	envCfg := envloader.
		NewLoader(metaData).
		WithDescriptionKey("desc").
		WithPrefix("app")

	jsonCfg := jsonloader.
		NewLoader(metaData).
		WithFiles(
			"./config.json",
			"${HOME}/.config/_testapp/config.json",
		)

	tomlCfg := tomlloader.
		NewLoader(metaData).
		WithFiles(
			"./config.toml",
			"${HOME}/.config/_testapp/config.toml",
		).
		WithDescriptionKey("desc")

	_ = flagSet.Parse(os.Args[1:])

	err = cfgmetadata.Apply(
		jsonCfg,
		tomlCfg,
		envCfg,
		flagsCfg,
	)
	if err != nil {
		log.Fatal(err)
	}

	if printEnv {
		err = envCfg.PrintFields(os.Stdout, envloader.EnvFileTemplate)
		if err != nil {
			log.Fatal(err)
		}
	} else if printEnvTable {
		err = envCfg.PrintFields(os.Stdout, envloader.FieldsTableTemplate)
		if err != nil {
			log.Fatal(err)
		}
	} else if printJson {
		err = jsonCfg.PrintConfig(os.Stdout)
		if err != nil {
			log.Fatal(err)
		}
	} else if printToml {
		err = tomlCfg.PrintConfig(os.Stdout)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		log.Printf("The config is: %#v", c)
	}
}
