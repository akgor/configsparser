package main

import (
	"log"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"bitbucket.org/akgor/configsparser/envloader"
	"bitbucket.org/akgor/configsparser/flagsloader"
)

type HttpConfig struct {
	Host string
	Port int
	TLS  bool
}

type DbConfig struct {
	Host string
	Port uint
	User string
	Pass string
}

type ExternalLoggerConfig struct {
	Url *string
}

type Config struct {
	Db        DbConfig
	Http      HttpConfig
	ExtLogger *ExternalLoggerConfig
}

func main() {
	c := &Config{}

	metaData, err := cfgmetadata.NewMetadata(c, cfgmetadata.Options{})
	if err != nil {
		log.Fatal(err)
	}

	err = cfgmetadata.Apply(
		envloader.NewLoader(metaData),
		flagsloader.NewLoader(metaData),
	)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("The config is: %#v", c)
}
