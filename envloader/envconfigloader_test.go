package envloader

import (
	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"testing"
)

func Test_LoadEnvConfig(t *testing.T) {
	var st struct {
		Int int
		Sub struct {
			SubSub struct {
				Int int
			}
		}
	}

	m, err := cfgmetadata.NewMetadata(&st, cfgmetadata.Options{})
	if err != nil {
		t.Fatal("Error is:", err)
	}

	envLoader := NewLoader(m).WithEnv([]string{
		"FOO_INT=43",
		"foo_Sub_SUBSub_Int=42",
	}).WithPrefix("Foo")

	t.Logf("Keys are: %#v", envLoader.GetPossibleKeys())

	err = envLoader.Load()
	if err != nil {
		t.Fatal("Error is:", err)
	}

	if st.Int != 43 {
		t.Errorf("Wrong value: %#v", st.Int)
	}

	if st.Sub.SubSub.Int != 42 {
		t.Errorf("Wrong value: %#v", st.Sub.SubSub.Int)
	}
}
