package envloader

import (
	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"io"
	"os"
	"strings"
)

func DefaultKeyNormalizer(s string) string {
	replacer := strings.NewReplacer(".", "_", "-", "_")
	return replacer.Replace(strings.ToUpper(strings.TrimSpace(s)))
}

type Loader struct {
	m              *cfgmetadata.MetaData
	Env            []string
	Normalizer     func(string) string
	Prefix         string
	DescriptionKey string
}

func NewLoader(m *cfgmetadata.MetaData) *Loader {
	return &Loader{
		m:          m,
		Normalizer: DefaultKeyNormalizer,
		Prefix:     "",
	}
}

func (cl *Loader) WithEnv(e []string) *Loader {
	cl.Env = e
	return cl
}

func (cl *Loader) WithNormalizer(f func(string) string) *Loader {
	cl.Normalizer = f
	return cl

}

func (cl *Loader) WithPrefix(p string) *Loader {
	cl.Prefix = p
	return cl
}

func (cl *Loader) WithDescriptionKey(key string) *Loader {
	cl.DescriptionKey = key
	return cl
}

func (cl *Loader) prefix() string {
	prefix := cl.Prefix

	if prefix != "" {
		if !strings.HasSuffix(prefix, "_") {
			prefix += "_"
		}
		prefix = cl.Normalizer(prefix)
	}

	return prefix
}

func (cl *Loader) Load() error {
	env := cl.Env
	if env == nil {
		env = os.Environ()
	}

	prefix := cl.prefix()

	// get map of normalized keys to keys of metadata fields
	keysMap := make(map[string]string)
	for _, k := range cl.m.GetKeys() {
		keysMap[cl.Normalizer(k)] = k
	}

	for _, e := range env {
		pair := strings.SplitN(e, "=", 2)
		l := pair[0]
		v := pair[1]

		l = cl.Normalizer(l)

		if !strings.HasPrefix(l, prefix) {
			continue
		}

		if prefix != "" {
			l = l[len(prefix):]
		}

		if k, has := keysMap[l]; has {
			err := cl.m.SetFieldValue(k, v)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (cl *Loader) GetPossibleKeys() []string {
	prefix := cl.prefix()

	res := make([]string, 0)
	for _, k := range cl.m.GetKeys() {
		k = prefix + cl.Normalizer(k)
		res = append(res, k)
	}

	return res
}

const FieldsTableTemplate = `{{- "" -}}
Allowed environment variables are:

{{ tableHeader '=' "Variable" "Field" "Type" "Value" "Description" }}
{{ range .Fields }}
	{{- .Key | toEnvName }}\t
	{{- .FullKey }}\t
	{{- .Type }}\t
	{{- .StringValue }}\t
	{{- . | getDesc }}
{{ end }}
{{- "" -}}`

const EnvFileTemplate = `# Allowed environment variables are

{{ range .Fields }}
	{{- $desc := . | getDesc }}
	{{- if .IsNil }}#{{ end }}export{{ " " }}
	{{- .Key | toEnvName }}={{ .StringValue }}\t# {{ .FullKey }}[{{ .Type }}]{{ if $desc }}: {{ $desc }}{{ end }}.
{{ end }}`

func (cl *Loader) PrintFields(w io.WriteCloser, t string) error {
	return cfgmetadata.PrintFieldsWithTemplate(cl.m, os.Stdout, t, map[string]interface{}{
		"toEnvName": func(s string) string {
			return cl.prefix() + cl.Normalizer(s)
		},
		"getDesc": func(f cfgmetadata.MetaDataField) string {
			if cl.DescriptionKey != "" {
				return f.Tag(cl.DescriptionKey)
			}
			return ""
		},
	})

}
