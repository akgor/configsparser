package fileloader

import (
	"fmt"
	"io"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"strings"
)

func GetAbsFilePath(s string, f func(string) string) (string, error) {
	s = os.Expand(s, f)
	if strings.HasPrefix(s, "~/") {
		usr, _ := user.Current()
		s = path.Join(usr.HomeDir, s[2:])
	}
	if pth, err := filepath.Abs(s); err != nil {
		return "", err
	} else {
		return pth, nil
	}
}

func LoadFile(fileName string, mustExist bool, loader func(r io.Reader) error) error {
	fn, err := GetAbsFilePath(fileName, os.Getenv)
	if err != nil {
		return err
	}

	f, err := os.Open(fn)
	if err != nil {
		if os.IsNotExist(err) && !mustExist {
			return nil
		}
		return err
	}
	defer f.Close()

	return loader(f)
}

func LoadFiles(fileNames []string, mustExist bool, loader func(r io.Reader) error) error {
	for _, fn := range fileNames {
		err := LoadFile(fn, mustExist, loader)
		if err != nil {
			return fmt.Errorf("cannot load config file %s: %w", fn, err)
		}
	}
	return nil
}
