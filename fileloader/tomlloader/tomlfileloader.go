package tomlloader

import (
	"io"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"bitbucket.org/akgor/configsparser/fileloader"
	"github.com/pelletier/go-toml"
)

type Loader struct {
	m     *cfgmetadata.MetaData
	names []string

	// Fail with error if file is not found
	FailIfNotExist bool

	DescriptionTag string
}

func NewLoader(m *cfgmetadata.MetaData, fileNames ...string) *Loader {
	return &Loader{
		m:     m,
		names: fileNames,
	}
}

// MustExist sets FailIfNotExist to true
func (fl *Loader) MustExist() *Loader {
	fl.FailIfNotExist = true
	return fl
}

func (fl *Loader) WithDescriptionKey(t string) *Loader {
	fl.DescriptionTag = t
	return fl
}

func (fl *Loader) WithFiles(fileNames ...string) *Loader {
	fl.names = append(fl.names, fileNames...)
	return fl
}

func (fl *Loader) Load() error {
	return fileloader.LoadFiles(fl.names, fl.FailIfNotExist, func(r io.Reader) error {
		tree, err := toml.LoadReader(r)
		if err != nil {
			return err
		}
		return tree.Unmarshal(fl.m.GetObject())
	})
}

func (fl *Loader) PrintConfig(w io.Writer) error {
	encoder := toml.NewEncoder(w)
	if fl.DescriptionTag != "" {
		encoder.SetTagComment(fl.DescriptionTag)
	}
	return encoder.Encode(fl.m.GetObject())
}
