package jsonloader_test

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"path"
	"runtime"
	"strings"
	"testing"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"bitbucket.org/akgor/configsparser/fileloader/jsonloader"
)

//
// Helpers
//

func getTestDir() string {
	_, filename, _, _ := runtime.Caller(0)
	return path.Dir(filename)
}

func getTestMetadata(t *testing.T, st interface{}, opts *cfgmetadata.Options) *cfgmetadata.MetaData {
	t.Helper()

	if opts == nil {
		opts = &cfgmetadata.Options{}
	}

	m, err := cfgmetadata.NewMetadata(st, *opts)
	if err != nil {
		t.Fatal(err)
	}

	return m
}

//
// Tests
//

func Test_LoadConfig(t *testing.T) {
	var st struct {
		AKey string
	}

	fileName := path.Join(getTestDir(), "testfiles", "test_conf.json")

	metadata := getTestMetadata(t, &st, nil)
	jsonFileLoader := jsonloader.NewLoader(metadata).WithFiles(fileName)

	err := jsonFileLoader.Load()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(fileName, st)

	if st.AKey != "foobar" {
		t.Errorf("Wrong value: %#v", st.AKey)
	}
}

func Test_FileIsMissing(t *testing.T) {
	var st struct {
		AKey string
	}

	fileName := path.Join(getTestDir(), "testfiles", "test_conf_missing.json")

	metadata := getTestMetadata(t, &st, nil)
	jsonFileLoader := jsonloader.NewLoader(metadata).WithFiles(fileName)

	err := jsonFileLoader.Load()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(fileName, st)

	if st.AKey != "" {
		t.Errorf("Wrong value: %#v", st.AKey)
	}
}

func Test_FileMustExist(t *testing.T) {
	var st struct {
		AKey string
	}

	fileName := path.Join(getTestDir(), "testfiles", "test_conf_missing.json")

	metadata := getTestMetadata(t, &st, nil)
	jsonFileLoader := jsonloader.NewLoader(metadata).WithFiles(fileName).MustExist()

	err := jsonFileLoader.Load()
	if !errors.Is(err, os.ErrNotExist) {
		t.Errorf("Wrong value: %#v", err)
	}

	strErr := fmt.Sprint(err)
	if !strings.Contains(strErr, "cannot load config file") {
		t.Errorf("Wrong value: %#v", strErr)
	}
	if !strings.Contains(strErr, "no such file or directory") {
		t.Errorf("Wrong value: %#v", strErr)
	}
}

func Test_PrintFile(t *testing.T) {
	var st struct {
		AKey string
	}
	st.AKey = "test"

	metadata := getTestMetadata(t, &st, nil)
	jsonFileLoader := jsonloader.NewLoader(metadata)

	buf := &bytes.Buffer{}
	jsonFileLoader.PrintConfig(buf)
	res := buf.String()

	desired := `{
    "AKey": "test"
}
`
	if res != desired {
		t.Errorf("Wrong value: %#v", res)
	}
}
