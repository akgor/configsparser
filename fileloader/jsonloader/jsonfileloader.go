package jsonloader

import (
	"encoding/json"
	"io"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
	"bitbucket.org/akgor/configsparser/fileloader"
)

type Loader struct {
	m     *cfgmetadata.MetaData
	names []string

	// Fail with error if file is not found
	FailIfNotExist bool
}

func NewLoader(m *cfgmetadata.MetaData, fileNames ...string) *Loader {
	return &Loader{
		m:     m,
		names: fileNames,
	}
}

// MustExist sets FailIfNotExist to true
func (fl *Loader) MustExist() *Loader {
	fl.FailIfNotExist = true
	return fl
}

func (fl *Loader) WithFiles(fileNames ...string) *Loader {
	fl.names = append(fl.names, fileNames...)
	return fl
}

func (fl *Loader) Load() error {
	return fileloader.LoadFiles(fl.names, fl.FailIfNotExist, func(r io.Reader) error {
		decoder := json.NewDecoder(r)
		return decoder.Decode(fl.m.GetObject())
	})
}

func (fl *Loader) PrintConfig(w io.Writer) error {
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "    ")
	return encoder.Encode(fl.m.GetObject())
}
