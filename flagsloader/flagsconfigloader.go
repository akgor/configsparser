package flagsloader

import (
	"flag"
	"fmt"
	"strings"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
)

func DefaultKeyNormalizer(s string) string {
	replacer := strings.NewReplacer(".", "-", "_", "-")
	return replacer.Replace(strings.ToLower(strings.TrimSpace(s)))
}

type value struct {
	key    string
	setter func(k, v string) error
}

func (value) String() string {
	return "boo" // fixme: implement something
}

func (v value) Set(val string) error {
	return v.setter(v.key, val)
}

type Loader struct {
	m      *cfgmetadata.MetaData
	s      *flag.FlagSet
	values map[string]string

	Normalizer     func(string) string
	DescriptionKey string
}

func NewLoader(m *cfgmetadata.MetaData) *Loader {
	return &Loader{
		m:      m,
		values: make(map[string]string),
	}
}

func (fl *Loader) WithFlagsSet(s *flag.FlagSet) *Loader {
	fl.s = s
	fl.init()
	return fl
}

func (fl *Loader) WithDescriptionKey(key string) *Loader {
	if fl.s != nil {
		panic("Flagset must not be set")
	}
	fl.DescriptionKey = key
	return fl
}

func (fl *Loader) init() {
	if fl.Normalizer == nil {
		fl.Normalizer = DefaultKeyNormalizer
	}

	for _, f := range fl.m.GetFields() {
		k := f.Key()
		fk := fl.Normalizer(k)

		val := value{
			key: k,
			setter: func(k, v string) error {
				fl.values[k] = v
				return nil
			},
		}

		var desc string
		if fl.DescriptionKey != "" {
			desc = f.Tag(fl.DescriptionKey)
		}
		if desc == "" {
			desc = fmt.Sprintf("Set configuration `value` for option '%s'", k)
		}

		fl.s.Var(val, fk, desc)
	}
}

func (fl *Loader) Load() error {
	// If flag set was not set, use flag command line by default
	if fl.s == nil {
		fl.s = flag.CommandLine
		fl.init()
		flag.Parse()
	}

	for k, v := range fl.values {
		err := fl.m.SetFieldValue(k, v)
		if err != nil {
			return err
		}
	}

	return nil
}
