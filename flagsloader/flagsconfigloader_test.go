package flagsloader

import (
	"bytes"
	"flag"
	"testing"

	"bitbucket.org/akgor/configsparser/cfgmetadata"
)

func Test_LoadFlagsConfig(t *testing.T) {
	var st struct {
		Int int
		Sub struct {
			SubSub struct {
				Int int
			}
		}
	}

	m, err := cfgmetadata.NewMetadata(&st, cfgmetadata.Options{})
	if err != nil {
		t.Fatal("Error is:", err)
	}

	fs := flag.NewFlagSet("", flag.ContinueOnError)
	loader := NewLoader(m).WithFlagsSet(fs)

	b := &bytes.Buffer{}
	fs.SetOutput(b)
	fs.PrintDefaults()
	t.Log("Usage is:\n", b.String())

	err = fs.Parse([]string{
		"-int",
		"43",
		"-sub-subsub-int=42",
	})
	if err != nil {
		t.Fatal("Error is:", err)
	}

	err = loader.Load()
	if err != nil {
		t.Fatal("Error is:", err)
	}

	if st.Int != 43 {
		t.Errorf("Wrong value: %#v", st.Int)
	}

	if st.Sub.SubSub.Int != 42 {
		t.Errorf("Wrong value: %#v", st.Sub.SubSub.Int)
	}
}
